from django import forms
from .models import Formulario


class FormularioForms(forms.ModelForm):

    class Meta:
        model= Formulario

        fields=[
            'nombre',
            'edad',
            'comentario',
            'foto',
        ]
        labels = {
            'nombre': 'Nombre',
            'edad': 'Edad',
            'comentario': 'Comentario',
            'foto': 'Foto',
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'edad': forms.TextInput(attrs={'class':'form-control'}),
            'comentario': forms.TextInput(attrs={'class':'form-control'}),
            #'foto': forms.ImageField('Fotografia', null = False, blank = False),
        }